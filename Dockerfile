FROM php:alpine as base

WORKDIR /application

RUN apk add --update-cache --allow-untrusted \
    make \
    python \
    g++ \
    libmcrypt-dev \
    openssl \
    openssl-dev \
    gmp-dev \
    libsasl \
    cyrus-sasl-dev \
    autoconf \
    php7-openssl \
    php7-dev \
    php7-mysqli \
    openssh-client \
    --repository http://dl-3.alpinelinux.org/alpine/edge/testing/ \
    --repository http://dl-3.alpinelinux.org/alpine/edge/main/ \
    --repository http://dl-3.alpinelinux.org/alpine/edge/testing/ \
    --repository http://dl-3.alpinelinux.org/alpine/edge/community/

RUN docker-php-ext-install pdo pdo_mysql

#RUN pecl install xdebug && docker-php-ext-enable xdebug

COPY . /application

RUN php /application/composer install

CMD ["artisan","serve", "--host=0.0.0.0", "--port=80"]

EXPOSE 80

ENTRYPOINT ["php"]