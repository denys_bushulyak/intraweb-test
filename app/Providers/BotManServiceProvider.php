<?php

namespace App\Providers;

use App\Http\Middleware\BotManMiddleware;
use BotMan\BotMan\BotMan;
use BotMan\BotMan\BotManFactory;
use BotMan\BotMan\Cache\LaravelCache;
use BotMan\BotMan\Drivers\DriverManager;
use BotMan\Drivers\Telegram\TelegramDriver;
use Illuminate\Support\ServiceProvider;

class BotManServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {

        $this->app->singleton(BotMan::class, function () {

            DriverManager::loadDriver(TelegramDriver::class);

            $botMan = BotManFactory::create(config('botman'));

//            $botMan->middleware->matching(new BotManMiddleware());

            return $botMan;
        });
    }
}
