<?php

namespace App\Providers;

use BotMan\BotMan\BotMan;
use Illuminate\Support\ServiceProvider;

class DomainInformation extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(DomainInformation::class, function () {
            return new self;
        });
    }

    /**
     * Return information about domain certificate.
     *
     * @param BotMan $bot
     *
     * @param string $domain
     */
    public function getInfo(BotMan $bot, string $domain):void
    {
        $bot-reply("Hello, but I am not implemented yet for talk about $domain");
    }
}
