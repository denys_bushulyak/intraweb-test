<?php
/**
 * User: denys
 * Date: 22.10.17
 * Time: 10:02
 */

namespace App\Http\Middleware;

use BotMan\BotMan\BotMan;
use BotMan\BotMan\Interfaces\MiddlewareInterface;
use BotMan\BotMan\Messages\Incoming\IncomingMessage;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class BotManMiddleware implements MiddlewareInterface
{
    /**
     * Handle a captured message.
     *
     * @param IncomingMessage $message
     * @param callable        $next
     * @param BotMan          $bot
     *
     * @return mixed
     */
    public function captured(IncomingMessage $message, $next, BotMan $bot)
    {

        $next($message);
    }

    /**
     * Handle a message that was successfully heard, but not processed yet.
     *
     * @param IncomingMessage $message
     * @param callable        $next
     * @param BotMan          $bot
     *
     * @return mixed
     */
    public function heard(IncomingMessage $message, $next, BotMan $bot)
    {

        $next($message);
    }

    /**
     * @param IncomingMessage $message
     * @param string          $pattern
     * @param bool            $regexMatched Indicator if the regular expression
     *                                      was matched too
     *
     * @return bool
     */
    public function matching(IncomingMessage $message, $pattern, $regexMatched)
    {

        Log::debug('Captured Message', [$message->getPayload()]);

        $messageId = $message->getPayload()->get('message_id');

        $isMessageAlreadyOperated = Cache::get('lastMessageId')
            >= $messageId;

        if ($isMessageAlreadyOperated) {
            return false;
        }

        $expireAt = Carbon::now()->addHour(24);

        Cache::put('lastMessageId', $messageId, $expireAt);

        return true;
    }

    /**
     * Handle an incoming message.
     *
     * @param IncomingMessage $message
     * @param callable        $next
     * @param BotMan          $bot
     *
     * @return mixed
     */
    public function received(IncomingMessage $message, $next, BotMan $bot)
    {

        $next($message);

    }

    /**
     * Handle an outgoing message payload before/after it
     * hits the message service.
     *
     * @param mixed    $payload
     * @param callable $next
     * @param BotMan   $bot
     *
     * @return mixed
     */
    public function sending($payload, $next, BotMan $bot)
    {

        $next($payload);
    }
}