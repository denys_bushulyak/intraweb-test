<?php

namespace App\Http\Controllers;

use App\Subscriber;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class SubscribersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Subscriber::all();
    }

    /**
     * Batch destroy
     * @param Request $request
     */
    public function batchDestroy(Request $request)
    {
        Subscriber::destroy($request->get('ids'));
    }
}
