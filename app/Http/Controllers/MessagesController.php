<?php

namespace App\Http\Controllers;

use App\Http\Requests\SendMessageRequest;
use BotMan\BotMan\BotMan;
use BotMan\Drivers\Telegram\TelegramDriver;
use Symfony\Component\HttpFoundation\Response;

class MessagesController extends Controller
{
    /**
     * @param SendMessageRequest $request
     * @param BotMan             $botMan
     *
     * @return Response
     * @throws \Exception
     */
    public function sendMessage(SendMessageRequest $request, BotMan $botMan)
    {

        /*
         * $botMan->say does not accept array as expected.
         * Telegram service return that chat_id is absent.
         * That is why I looping it.
         */

        foreach ($request->get('ids') as $recipientId) {
            /** @var Response $response */
            $botMan->say($request->get('message'),
                $recipientId,
                TelegramDriver::class
            );
        }
    }
}
