<?php

namespace App\Http\Controllers;

use App\Http\Requests\BotRequest;
use App\Subscriber;
use BotMan\BotMan\BotMan;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Spatie\SslCertificate\SslCertificate;

class BotmanController extends Controller
{
    public function handleBotWebhook(BotRequest $request, BotMan $botMan)
    {

        Log::debug('Bot message', $request->toArray());

        $botMan->hears('ssl-info {domain}', function (BotMan $botMan,
            $domain) {

            try {

                $certificate = SslCertificate::createForHostName($domain);

                $isValid   = $certificate->isValid() ? 'True' : "False";
                $issuer    = $certificate->getIssuer();
                $expiredIn = $certificate->expirationDate()->diffInDays();
                $days      = Str::plural('day', $expiredIn);

                $botMan->reply("Issuer: $issuer");
                $botMan->reply("Is Valid: $isValid");
                $botMan->reply("Expired In: $expiredIn $days");

            } catch (\Exception $e) {
                $botMan->reply("Error! Check domain again.");
            }
        });

        $botMan->hears('subscribe', function (BotMan $botMan) {

            $username = sprintf("%s %s",
                $botMan->getUser()->getFirstName(),
                $botMan->getUser()->getLastName());

            Subscriber::updateOrCreate([
                'id' => $botMan->getUser()->getId(),
            ], [
                'username' => $username,
            ]);

            $botMan->reply("You have already subscribed.");
        });

        $botMan->hears('unsubscribe', function (BotMan $botMan) {

            Subscriber::destroy($botMan->getUser()->getId());

            $botMan->reply("You unsubscribed.");

        });

        $botMan->fallback(function (BotMan $botMan){
            $botMan->reply("Sorry, did not understand.");
        });

        $botMan->listen();

        return new Response();
    }
}
