<?php

use function foo\func;
use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\Artisan;
use Symfony\Component\HttpKernel\Client;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');

Artisan::command('botman:register', function(){

    $telegramToken = config('botman.telegram.token');

    $url = "https://api.telegram.org/bot$telegramToken/setWebhook";

    $ch = curl_init($url);

    curl_setopt_array($ch, [
        CURLOPT_RETURNTRANSFER=>true,
        CURLOPT_POSTFIELDS=>[
            'url'=> route('botman-webhook-entrypoint',[
                'token'=>config('botman.webhook_token')
            ],true)
        ]
    ]);

    $result = curl_exec($ch);

    $responseBody = json_decode($result);

    if($responseBody->ok){
        $this->info("Telegram webhook registered.");
    }else{
        $this->error("Telegram webhook not registered. Code:"
            .$responseBody->error_code);
        $this->error($responseBody->description);
    }
});