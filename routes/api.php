<?php

use function foo\func;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/botman/{token}',
'BotmanController@handleBotWebhook')
    ->name('botman-webhook-entrypoint');

Route::resource('subscribers', 'SubscribersController');
Route::post('/sendMessage', 'MessagesController@sendMessage');
Route::post('/subscribers/batch','SubscribersController@batchDestroy');