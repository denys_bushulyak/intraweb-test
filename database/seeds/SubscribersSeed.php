<?php

use App\Subscriber;

use Illuminate\Database\Seeder;

class SubscribersSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Subscriber::class)->times(20)->create();
    }
}
